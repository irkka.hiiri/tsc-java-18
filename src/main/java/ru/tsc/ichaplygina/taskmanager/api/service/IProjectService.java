package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService {

    List<Project> findAll(Comparator<Project> comparator) throws AbstractException;

    void completeById(final String id) throws AbstractException;

    void completeByIndex(final int index) throws AbstractException;

    void completeByName(final String name) throws AbstractException;

    Project add(String name, String description) throws AbstractException;

    Project findById(final String id) throws AbstractException;

    Project findByName(final String name) throws AbstractException;

    Project findByIndex(final int index) throws AbstractException;

    boolean isEmpty() throws AbstractException;

    void startById(final String id) throws AbstractException;

    void startByIndex(final int index) throws AbstractException;

    void startByName(final String name) throws AbstractException;

    void updateByIndex(final int index, final String name, final String description) throws AbstractException;

    void updateById(final String id, final String name, final String description) throws AbstractException;

    void updateStatus(String id, Status status) throws AbstractException;

}
