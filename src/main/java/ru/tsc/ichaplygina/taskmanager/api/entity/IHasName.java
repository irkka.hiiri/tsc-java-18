package ru.tsc.ichaplygina.taskmanager.api.entity;

public interface IHasName {

    String getName();

    void setName(String name);

}
