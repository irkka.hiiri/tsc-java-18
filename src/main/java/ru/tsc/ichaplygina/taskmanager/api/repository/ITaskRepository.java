package ru.tsc.ichaplygina.taskmanager.api.repository;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    boolean isNotFoundByIdForUser(String id, String userId);

    List<Task> findAll();

    List<Task> findAllForUser(String userId);

    List<Task> findAll(Comparator<Task> comparator);

    List<Task> findAllForUser(Comparator<Task> comparator, String userId);

    void add(final Task task);

    Task update(String id, String name, String description);

    void clear();

    void clearForUser(String currentUserId);

    Task findById(final String id);

    Task findByIdForUser(String id, String userId);

    Task findByName(final String name);

    Task findByNameForUser(String name, String userId);

    Task findByIndex(final int index);

    String getId(String name);

    String getId(int index);

    int getSize();

    int getSizeForUser(String userId);

    boolean isEmpty();

    boolean isEmptyForUser(String userId);

    boolean isNotFoundById(String id);

    List<Task> findAllByProjectId(String projectId);

    List<Task> findAllByProjectIdForUser(String projectId, String userId);

    List<Task> findAllByProjectId(String projectId, Comparator<Task> comparator);

    List<Task> findAllByProjectIdForUser(String projectId, Comparator<Task> comparator, String userId);

    boolean isNotFoundTaskInProject(String taskId, String projectId);

    void remove(final Task task);

    void removeAllByProjectId(final String projectId);

    Task findByIndexForUser(int index, String userId);

    Task removeById(final String id);

    Task removeByIdForUser(String id, String userId);

    Task removeByIndex(final int index);

    Task removeByIndexForUser(int index, String userId);

    Task removeByName(final String name);

    Task removeByNameForUser(String name, String userId);

    Task updateStatus(String id, Status status);

    void updateProjectId(final String taskId, final String projectId);

    String getIdForUser(int index, String userId);
}
