package ru.tsc.ichaplygina.taskmanager.api.repository;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    boolean isEmptyForUser(String userId);

    boolean isNotFoundById(String id);

    boolean isNotFoundByIdForUser(String id, String userId);

    void add(final Project project);

    Project update(String id, String name, String description);

    void clear();

    void clearForUser(String currentUserId);

    Project findById(final String id);

    Project findByIdForUser(String id, String userId);

    Project findByName(final String name);

    Project findByNameForUser(String name, String userId);

    Project findByIndex(final int index);

    String getId(final String name);

    String getId(final int index);

    List<Project> findAll();

    List<Project> findAllForUser(String userId);

    List<Project> findAll(Comparator<Project> comparator);

    List<Project> findAllForUser(Comparator<Project> comparator, String userId);

    int getSize();

    int getSizeForUser(String userId);

    boolean isEmpty();

    void remove(final Project project);

    void updateStatus(String id, Status status);

    Project findByIndexForUser(int index, String userId);

    Project removeById(final String id);

    Project removeByIdForUser(String id, String userId);

    String getIdForUser(int index, String userId);

    String getIdForUser(String name, String userId);

}
