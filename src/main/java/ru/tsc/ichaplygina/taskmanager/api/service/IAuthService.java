package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public interface IAuthService {

    String getCurrentUserId();

    void setCurrentUserId(final String currentUserId) throws AbstractException;

    boolean isNoUserLoggedIn();

    void login(String login, String password) throws AbstractException;

    void logout() throws AbstractException;

    boolean isAdmin() throws AbstractException;
}
