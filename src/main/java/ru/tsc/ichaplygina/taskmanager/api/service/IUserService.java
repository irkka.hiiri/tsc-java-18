package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.List;

public interface IUserService {
    void add(String login, String password, String email, Role role,
             String firstName, String middleName, String lastName) throws AbstractException;

    boolean isEmpty();

    User findByLogin(String login) throws AbstractException;

    List<User> findAll();

    User findById(String id) throws AbstractException;

    void updateById(String id, String login, String password, String email,
                    Role role, String firstName, String middleName, String lastName) throws AbstractException;

    void updateByLogin(String login, String password, String email,
                       Role role, String firstName, String middleName, String lastName) throws AbstractException;

    void setPassword(String login, String password) throws AbstractException;

    void setRole(String login, Role role) throws AbstractException;

    void removeById(String id) throws AbstractException;

    void removeByLogin(String login) throws AbstractException;
}
