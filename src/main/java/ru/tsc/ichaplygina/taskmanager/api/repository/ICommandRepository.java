package ru.tsc.ichaplygina.taskmanager.api.repository;

import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import java.util.Map;

public interface ICommandRepository {

    Map<String, AbstractCommand> getCommands();

    Map<String, AbstractCommand> getArguments();

}
