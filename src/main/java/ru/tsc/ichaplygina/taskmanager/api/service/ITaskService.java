package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void clear() throws AbstractException;

    void completeById(final String id) throws AbstractException;

    void completeByIndex(final int index) throws AbstractException;

    void completeByName(final String name) throws AbstractException;

    List<Task> findAll(final Comparator<Task> comparator) throws AbstractException;

    Task add(String name, String description) throws AbstractException;

    Task findById(final String id) throws AbstractException;

    Task findByName(final String name) throws AbstractException;

    Task findByIndex(final int index) throws AbstractException;

    int getSize() throws AbstractException;

    boolean isEmpty() throws AbstractException;

    Task removeById(final String id) throws AbstractException;

    Task removeByIndex(final int index) throws AbstractException;

    Task removeByName(final String name) throws AbstractException;

    void startById(final String id) throws AbstractException;

    void startByIndex(final int index) throws AbstractException;

    Task startByName(final String name) throws AbstractException;

    void updateByIndex(final int index, final String name, final String description) throws AbstractException;

    Task updateById(final String id, final String name, final String description) throws AbstractException;

    Task updateStatus(String id, Status status) throws AbstractException;

}
