package ru.tsc.ichaplygina.taskmanager.exception.incorrect;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class IncorrectCredentialsException extends AbstractException {

    private static final String MESSAGE = "Error! Incorrect credentials.";

    public IncorrectCredentialsException() {
        super(MESSAGE);
    }

}
