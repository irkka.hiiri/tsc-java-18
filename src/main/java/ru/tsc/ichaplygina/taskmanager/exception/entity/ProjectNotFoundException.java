package ru.tsc.ichaplygina.taskmanager.exception.entity;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    private static final String MESSAGE = "Project not found.";

    public ProjectNotFoundException() {
        super(MESSAGE);
    }

}
