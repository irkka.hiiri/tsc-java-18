package ru.tsc.ichaplygina.taskmanager.exception.empty;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

public class PasswordEmptyException extends AbstractException {

    private static final String MESSAGE = "Error! Password is empty.";

    public PasswordEmptyException() {
        super(MESSAGE);
    }

}
