package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    private final IAuthService authService;

    public ProjectService(final IProjectRepository projectRepository, final IAuthService authService) {
        this.projectRepository = projectRepository;
        this.authService = authService;
    }

    private List<Project> findAllForAdmin(final Comparator<Project> comparator) {
        if (comparator == null) return projectRepository.findAll();
        return projectRepository.findAll(comparator);
    }

    private List<Project> findAllForUser(final Comparator<Project> comparator, final String userId) {
        if (comparator == null) return projectRepository.findAllForUser(userId);
        return projectRepository.findAllForUser(comparator, userId);
    }

    private Project findByIndexForAdmin(final int index) throws AbstractException {
        if (isInvalidListIndex(index, projectRepository.getSize())) throw new IndexIncorrectException(index + 1);
        return projectRepository.findByIndex(index);
    }

    private Project findByIndexForUser(final int index, final String userId) throws AbstractException {
        if (isInvalidListIndex(index, projectRepository.getSizeForUser(userId)))
            throw new IndexIncorrectException(index + 1);
        return projectRepository.findByIndexForUser(index, userId);
    }

    private void updateByIdForAdmin(final String id, final String name, final String description) throws AbstractException {
        if (projectRepository.isNotFoundById(id)) throw new ProjectNotFoundException();
        projectRepository.update(id, name, description);
    }

    private void updateByIdForUser(final String id, final String name, final String description, final String userId) throws AbstractException {
        if (projectRepository.isNotFoundByIdForUser(id, userId)) throw new ProjectNotFoundException();
        projectRepository.update(id, name, description);
    }

    private void updateByIndexForAdmin(final int index, final String name, final String description) throws AbstractException {
        if (isInvalidListIndex(index, projectRepository.getSize())) throw new IndexIncorrectException(index + 1);
        final String id = projectRepository.getId(index);
        if (projectRepository.isNotFoundById(id)) throw new ProjectNotFoundException();
        updateById(id, name, description);
    }

    private void updateByIndexForUser(final int index, final String name, final String description, final String userId) throws AbstractException {
        if (isInvalidListIndex(index, projectRepository.getSizeForUser(userId)))
            throw new IndexIncorrectException(index + 1);
        final String id = projectRepository.getIdForUser(index, userId);
        if (projectRepository.isNotFoundByIdForUser(id, userId)) throw new ProjectNotFoundException();
        updateById(id, name, description);
    }

    private void updateStatusForAdmin(final String id, final Status status) throws AbstractException {
        if (projectRepository.isNotFoundById(id)) throw new ProjectNotFoundException();
        projectRepository.updateStatus(id, status);
    }

    private void updateStatusForUser(final String id, final Status status, final String userId) throws AbstractException {
        if (projectRepository.isNotFoundByIdForUser(id, userId)) throw new ProjectNotFoundException();
        projectRepository.updateStatus(id, status);
    }

    @Override
    public boolean isEmpty() throws AbstractException {
        if (authService.isAdmin()) return projectRepository.isEmpty();
        return projectRepository.isEmptyForUser(authService.getCurrentUserId());
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) throws AbstractException {
        if (authService.isAdmin()) return findAllForAdmin(comparator);
        return findAllForUser(comparator, authService.getCurrentUserId());
    }

    @Override
    public Project add(final String name, final String description) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final Project project = new Project(name, description, authService.getCurrentUserId());
        projectRepository.add(project);
        return project;
    }

    @Override
    public Project findById(final String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (authService.isAdmin()) return projectRepository.findById(id);
        return projectRepository.findByIdForUser(id, authService.getCurrentUserId());
    }

    @Override
    public Project findByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        if (authService.isAdmin()) return projectRepository.findByName(name);
        return projectRepository.findByNameForUser(name, authService.getCurrentUserId());
    }

    @Override
    public Project findByIndex(final int index) throws AbstractException {
        if (authService.isAdmin()) return findByIndexForAdmin(index);
        return findByIndexForUser(index, authService.getCurrentUserId());
    }

    @Override
    public void updateById(final String id, final String name, final String description) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (isEmptyString(name)) throw new NameEmptyException();
        if (authService.isAdmin()) updateByIdForAdmin(id, name, description);
        else updateByIdForUser(id, name, description, authService.getCurrentUserId());
    }

    @Override
    public void updateByIndex(final int index, final String name, final String description) throws AbstractException {
        if (authService.isAdmin()) updateByIndexForAdmin(index, name, description);
        else updateByIndexForUser(index, name, description, authService.getCurrentUserId());
    }


    @Override
    public void updateStatus(final String id, final Status status) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (authService.isAdmin()) updateStatusForAdmin(id, status);
        else updateStatusForUser(id, status, authService.getCurrentUserId());
    }

    @Override
    public void startById(String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public void startByIndex(int index) throws AbstractException {
        final boolean isAdmin = authService.isAdmin();
        final String userId = authService.getCurrentUserId();
        final int size = isAdmin ? projectRepository.getSize() : projectRepository.getSizeForUser(userId);
        if (isInvalidListIndex(index, size)) throw new IndexIncorrectException(index + 1);
        final String id = isAdmin ? projectRepository.getId(index) : projectRepository.getIdForUser(index, userId);
        updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public void startByName(String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final String id = projectRepository.getId(name);
        if (id == null) throw new ProjectNotFoundException();
        updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public void completeById(String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        updateStatus(id, Status.COMPLETED);
    }

    @Override
    public void completeByIndex(int index) throws AbstractException {
        final boolean isAdmin = authService.isAdmin();
        final String userId = authService.getCurrentUserId();
        final int size = isAdmin ? projectRepository.getSize() : projectRepository.getSizeForUser(userId);
        if (isInvalidListIndex(index, size)) throw new IndexIncorrectException(index + 1);
        final String id = isAdmin ? projectRepository.getId(index) : projectRepository.getIdForUser(index, userId);
        updateStatus(id, Status.COMPLETED);
    }

    @Override
    public void completeByName(String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final String id = projectRepository.getId(name);
        if (id == null) throw new ProjectNotFoundException();
        updateStatus(id, Status.COMPLETED);
    }

}
