package ru.tsc.ichaplygina.taskmanager.service;

import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.ITaskService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;
import java.util.List;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.*;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    private final IAuthService authService;

    public TaskService(final ITaskRepository taskRepository, final IAuthService authService) {
        this.taskRepository = taskRepository;
        this.authService = authService;
    }

    private List<Task> findAllForAdmin(final Comparator comparator) {
        if (comparator == null) return taskRepository.findAll();
        return taskRepository.findAll(comparator);
    }

    private List<Task> findAllForUser(final Comparator comparator, final String userId) {
        if (comparator == null) return taskRepository.findAllForUser(authService.getCurrentUserId());
        return taskRepository.findAllForUser(comparator, userId);
    }

    private Task findByIndexForAdmin(final int index) throws AbstractException {
        if (isInvalidListIndex(index, taskRepository.getSize())) throw new IndexIncorrectException(index + 1);
        return taskRepository.findByIndex(index);
    }

    private Task findByIndexForUser(final int index, final String userId) throws AbstractException {
        if (isInvalidListIndex(index, taskRepository.getSizeForUser(userId)))
            throw new IndexIncorrectException(index + 1);
        return taskRepository.findByIndexForUser(index, userId);
    }

    private Task removeByIndexForAdmin(final int index) throws AbstractException {
        if (isInvalidListIndex(index, taskRepository.getSize())) throw new IndexIncorrectException(index + 1);
        return taskRepository.removeByIndex(index);
    }

    private Task removeByIndexForUser(final int index, final String userId) throws AbstractException {
        if (isInvalidListIndex(index, taskRepository.getSizeForUser(userId)))
            throw new IndexIncorrectException(index + 1);
        return taskRepository.removeByIndexForUser(index, userId);
    }

    private Task updateByIdForAdmin(final String id, final String name, final String description) throws AbstractException {
        if (taskRepository.isNotFoundById(id)) throw new TaskNotFoundException();
        return taskRepository.update(id, name, description);
    }

    private Task updateByIdForUser(final String id, final String name, final String description, final String userId) throws AbstractException {
        if (taskRepository.isNotFoundByIdForUser(id, userId)) throw new TaskNotFoundException();
        return taskRepository.update(id, name, description);
    }

    private void updateByIndexForAdmin(final int index, final String name, final String description) throws AbstractException {
        if (isInvalidListIndex(index, taskRepository.getSize())) throw new IndexIncorrectException(index + 1);
        final String id = taskRepository.getId(index);
        if (taskRepository.isNotFoundById(id)) throw new TaskNotFoundException();
        updateById(id, name, description);
    }

    private void updateByIndexForUser(final int index, final String name, final String description, final String userId) throws AbstractException {
        if (isInvalidListIndex(index, taskRepository.getSizeForUser(userId)))
            throw new IndexIncorrectException(index + 1);
        final String id = taskRepository.getIdForUser(index, userId);
        if (taskRepository.isNotFoundByIdForUser(id, userId)) throw new TaskNotFoundException();
        updateById(id, name, description);
    }

    private Task updateStatusForAdmin(final String id, final Status status) throws AbstractException {
        if (taskRepository.isNotFoundById(id)) throw new TaskNotFoundException();
        return taskRepository.updateStatus(id, status);
    }

    private Task updateStatusForUser(final String id, final Status status, final String userId) throws AbstractException {
        if (taskRepository.isNotFoundByIdForUser(id, userId)) throw new TaskNotFoundException();
        return taskRepository.updateStatus(id, status);
    }

    @Override
    public int getSize() throws AbstractException {
        if (authService.isAdmin()) return taskRepository.getSize();
        return taskRepository.getSizeForUser(authService.getCurrentUserId());
    }

    @Override
    public boolean isEmpty() throws AbstractException {
        if (authService.isAdmin()) return taskRepository.isEmpty();
        return taskRepository.isEmptyForUser(authService.getCurrentUserId());
    }

    @Override
    public List<Task> findAll(final Comparator comparator) throws AbstractException {
        if (authService.isAdmin()) return findAllForAdmin(comparator);
        return findAllForUser(comparator, authService.getCurrentUserId());
    }

    @Override
    public Task add(final String name, final String description) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final Task task = new Task(name, description, authService.getCurrentUserId());
        taskRepository.add(task);
        return task;
    }

    @Override
    public void clear() throws AbstractException {
        if (authService.isAdmin()) {
            taskRepository.clear();
            return;
        }
        taskRepository.clearForUser(authService.getCurrentUserId());
    }

    @Override
    public Task findById(final String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (authService.isAdmin()) return taskRepository.findById(id);
        return taskRepository.findByIdForUser(id, authService.getCurrentUserId());
    }

    @Override
    public Task findByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        if (authService.isAdmin()) return taskRepository.findByName(name);
        return taskRepository.findByNameForUser(name, authService.getCurrentUserId());
    }

    @Override
    public Task findByIndex(final int index) throws AbstractException {
        if (authService.isAdmin()) return findByIndexForAdmin(index);
        return findByIndexForUser(index, authService.getCurrentUserId());
    }

    @Override
    public Task removeById(final String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (authService.isAdmin()) return taskRepository.removeById(id);
        return taskRepository.removeByIdForUser(id, authService.getCurrentUserId());
    }

    @Override
    public Task removeByIndex(final int index) throws AbstractException {
        if (authService.isAdmin()) return removeByIndexForAdmin(index);
        return removeByIndexForUser(index, authService.getCurrentUserId());
    }

    @Override
    public Task removeByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        if (authService.isAdmin()) return taskRepository.removeByName(name);
        return taskRepository.removeByNameForUser(name, authService.getCurrentUserId());
    }

    @Override
    public Task updateById(final String id, final String name, final String description) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (isEmptyString(name)) throw new NameEmptyException();
        if (authService.isAdmin()) return updateByIdForAdmin(id, name, description);
        return updateByIdForUser(id, name, description, authService.getCurrentUserId());
    }

    @Override
    public void updateByIndex(final int index, final String name, final String description) throws AbstractException {
        if (authService.isAdmin()) {
            updateByIndexForAdmin(index, name, description);
            return;
        }
        updateByIndexForUser(index, name, description, authService.getCurrentUserId());
    }

    @Override
    public Task updateStatus(final String id, final Status status) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        if (authService.isAdmin()) return updateStatusForAdmin(id, status);
        return updateStatusForUser(id, status, authService.getCurrentUserId());
    }

    @Override
    public void startById(final String id) throws AbstractException {
        updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public void startByIndex(int index) throws AbstractException {
        final boolean isAdmin = authService.isAdmin();
        final String userId = authService.getCurrentUserId();
        final int size = isAdmin ? taskRepository.getSize() : taskRepository.getSizeForUser(userId);
        if (isInvalidListIndex(index, size)) throw new IndexIncorrectException(index + 1);
        final String id = isAdmin ? taskRepository.getId(index) : taskRepository.getIdForUser(index, userId);
        updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public Task startByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final String id = taskRepository.getId(name);
        if (id == null) throw new TaskNotFoundException();
        return updateStatus(id, Status.IN_PROGRESS);
    }

    @Override
    public void completeById(final String id) throws AbstractException {
        if (isEmptyString(id)) throw new IdEmptyException();
        updateStatus(id, Status.COMPLETED);
    }

    @Override
    public void completeByIndex(int index) throws AbstractException {
        final boolean isAdmin = authService.isAdmin();
        final String userId = authService.getCurrentUserId();
        final int size = isAdmin ? taskRepository.getSize() : taskRepository.getSizeForUser(userId);
        if (isInvalidListIndex(index, size)) throw new IndexIncorrectException(index + 1);
        final String id = isAdmin ? taskRepository.getId(index) : taskRepository.getIdForUser(index, userId);
        updateStatus(id, Status.COMPLETED);
    }

    @Override
    public void completeByName(final String name) throws AbstractException {
        if (isEmptyString(name)) throw new NameEmptyException();
        final String id = taskRepository.getId(name);
        if (id == null) throw new TaskNotFoundException();
        updateStatus(id, Status.COMPLETED);
    }

}
