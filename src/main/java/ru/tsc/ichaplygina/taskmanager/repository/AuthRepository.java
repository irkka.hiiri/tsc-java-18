package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.IAuthRepository;

public class AuthRepository implements IAuthRepository {

    private String currentUserId;

    @Override
    public String getCurrentUserId() {
        return currentUserId;
    }

    @Override
    public void setCurrentUserId(final String currentUserId) {
        this.currentUserId = currentUserId;
    }
}
