package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepository implements IUserRepository {

    private final List<User> list = new ArrayList<>();

    @Override
    public void add(final User user) {
        list.add(user);
    }

    @Override
    public List<User> findAll() {
        return list;
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public User findByLogin(final String login) {
        for (User user : list) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findById(final String id) {
        for (User user : list) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (User user : list) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public boolean isFoundByLogin(final String login) {
        return (findByLogin(login)) != null;
    }

    @Override
    public boolean isFoundByEmail(final String email) {
        return (findByEmail(email)) != null;
    }

    @Override
    public void remove(final User user) {
        list.remove(user);
    }

    @Override
    public void removeById(final String id) {
        final User user = findById(id);
        if (user != null) remove(user);
    }

    @Override
    public void removeByLogin(final String login) {
        final User user = findByLogin(login);
        if (user != null) remove(user);
    }

    @Override
    public User update(final String id, final String login, final String password, final String firstName, final String middleName, final String lastName) {
        final User user = findById(id);
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

}
