package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> list = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return list;
    }

    @Override
    public List<Project> findAllForUser(final String userId) {
        final List<Project> list = new ArrayList<>();
        for (Project project : this.list) {
            if (project.getUserId().equals(userId)) list.add(project);
        }
        return list;
    }

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        List<Project> list = findAll();
        list.sort(comparator);
        return list;
    }

    @Override
    public List<Project> findAllForUser(Comparator<Project> comparator, final String userId) {
        final List<Project> list = findAllForUser(userId);
        list.sort(comparator);
        return list;
    }

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public int getSizeForUser(final String userId) {
        final List<Project> list = findAllForUser(userId);
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean isEmptyForUser(final String userId) {
        final List<Project> list = findAllForUser(userId);
        return list.isEmpty();
    }

    @Override
    public boolean isNotFoundById(final String id) {
        return findById(id) == null;
    }

    @Override
    public boolean isNotFoundByIdForUser(final String id, final String userId) {
        return findByIdForUser(id, userId) == null;
    }

    @Override
    public void add(final Project project) {
        list.add(project);
    }

    @Override
    public void remove(final Project project) {
        list.remove(project);
    }

    @Override
    public Project update(final String id, final String name, final String description) {
        final Project project = findById(id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public void clearForUser(final String currentUserId) {
        for (Project project : findAllForUser(currentUserId)) remove(project);
    }


    @Override
    public Project findById(final String id) {
        for (Project project : list) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByIdForUser(final String id, final String userId) {
        for (Project project : list) {
            if (id.equals(project.getId()) && userId.equals(project.getUserId())) return project;
        }
        return null;
    }

    @Override
    public Project findByName(final String name) {
        for (Project project : list) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findByNameForUser(final String name, final String userId) {
        for (Project project : list) {
            if (name.equals(project.getName()) && userId.equals(project.getUserId())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final int index) {
        return list.get(index);
    }

    @Override
    public Project findByIndexForUser(final int index, final String userId) {
        final List<Project> list = findAllForUser(userId);
        return list.get(index);
    }

    @Override
    public Project removeById(final String id) {
        final Project project = findById(id);
        if (project == null) return null;
        this.remove(project);
        return project;
    }

    @Override
    public Project removeByIdForUser(final String id, final String userId) {
        final Project project = findByIdForUser(id, userId);
        if (project == null) return null;
        remove(project);
        return project;
    }

    @Override
    public void updateStatus(final String id, final Status status) {
        final Project project = findById(id);
        project.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) project.setDateStart(new Date());
        else if (status.equals(Status.COMPLETED)) project.setDateFinish(new Date());
    }

    @Override
    public String getId(final String name) {
        Project project = findByName(name);
        if (project == null) return null;
        return project.getId();
    }

    @Override
    public String getId(final int index) {
        Project project = findByIndex(index);
        if (project == null) return null;
        return project.getId();
    }

    @Override
    public String getIdForUser(final int index, final String userId) {
        Project project = findByIndexForUser(index, userId);
        if (project == null) return null;
        return project.getId();
    }

    @Override
    public String getIdForUser(final String name, final String userId) {
        Project project = findByNameForUser(name, userId);
        if (project == null) return null;
        return project.getId();
    }

}
