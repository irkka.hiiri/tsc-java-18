package ru.tsc.ichaplygina.taskmanager.repository;

import ru.tsc.ichaplygina.taskmanager.api.repository.ITaskRepository;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public int getSizeForUser(final String userId) {
        final List<Task> list = findAllForUser(userId);
        return list.size();
    }

    @Override
    public boolean isEmpty() {
        return list.isEmpty();
    }

    @Override
    public boolean isEmptyForUser(final String userId) {
        final List<Task> list = findAllForUser(userId);
        return list.isEmpty();
    }

    @Override
    public boolean isNotFoundById(final String id) {
        return findById(id) == null;
    }

    @Override
    public boolean isNotFoundByIdForUser(final String id, final String userId) {
        return findByIdForUser(id, userId) == null;
    }

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public List<Task> findAllForUser(final String userId) {
        final List<Task> list = new ArrayList<>();
        for (Task task : this.list) {
            if (task.getUserId().equals(userId)) list.add(task);
        }
        return list;
    }

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> list = findAll();
        list.sort(comparator);
        return list;
    }

    @Override
    public List<Task> findAllForUser(Comparator<Task> comparator, final String userId) {
        final List<Task> list = findAllForUser(userId);
        list.sort(comparator);
        return list;
    }

    @Override
    public void add(final Task task) {
        list.add(task);
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public Task update(final String id, final String name, final String description) {
        final Task task = findById(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public void clearForUser(final String currentUserId) {
        for (Task task : findAllForUser(currentUserId)) remove(task);
    }

    @Override
    public Task findById(final String id) {
        for (Task task : list) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByIdForUser(final String id, final String userId) {
        for (Task task : list) {
            if (id.equals(task.getId()) && userId.equals(task.getUserId())) return task;
        }
        return null;
    }

    @Override
    public Task findByName(final String name) {
        for (Task task : list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findByNameForUser(final String name, final String userId) {
        for (Task task : list) {
            if (name.equals(task.getName()) && userId.equals(task.getUserId())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final int index) {
        return list.get(index);
    }

    @Override
    public Task findByIndexForUser(final int index, final String userId) {
        final List<Task> list = findAllForUser(userId);
        return list.get(index);
    }

    @Override
    public Task removeById(final String id) {
        final Task task = findById(id);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeByIdForUser(final String id, final String userId) {
        final Task task = findByIdForUser(id, userId);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final int index) {
        return list.remove(index);
    }

    @Override
    public Task removeByIndexForUser(final int index, final String userId) {
        final Task task = findByIndexForUser(index, userId);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        final Task task = findByName(name);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeByNameForUser(final String name, final String userId) {
        final Task task = findByNameForUser(name, userId);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task updateStatus(final String id, final Status status) {
        final Task task = findById(id);
        task.setStatus(status);
        if (status.equals(Status.IN_PROGRESS)) task.setDateStart(new Date());
        else if (status.equals(Status.COMPLETED)) task.setDateFinish(new Date());
        return task;
    }

    @Override
    public void updateProjectId(final String taskId, final String projectId) {
        final Task task = findById(taskId);
        task.setProjectId(projectId);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        List<Task> list = new ArrayList<>();
        for (Task task : this.list) {
            if (projectId.equals(task.getProjectId())) list.add(task);
        }
        return list;
    }

    @Override
    public List<Task> findAllByProjectIdForUser(final String projectId, final String userId) {
        List<Task> list = new ArrayList<>();
        for (Task task : findAllForUser(userId)) {
            if (projectId.equals(task.getProjectId())) list.add(task);
        }
        return list;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId, Comparator<Task> comparator) {
        final List<Task> list = findAllByProjectId(projectId);
        list.sort(comparator);
        return list;
    }

    @Override
    public List<Task> findAllByProjectIdForUser(final String projectId, Comparator<Task> comparator, final String userId) {
        final List<Task> list = findAllByProjectIdForUser(projectId, userId);
        list.sort(comparator);
        return list;
    }

    @Override
    public boolean isNotFoundTaskInProject(final String taskId, final String projectId) {
        return (!findAllByProjectId(projectId).contains(findById(taskId)));
    }

    @Override
    public void removeAllByProjectId(final String projectId) {
        list.removeIf(task -> projectId.equals(task.getProjectId()));
    }

    @Override
    public String getId(final String name) {
        Task task = findByName(name);
        if (task == null) return null;
        return task.getId();
    }

    @Override
    public String getId(final int index) {
        Task task = findByIndex(index);
        if (task == null) return null;
        return task.getId();
    }

    @Override
    public String getIdForUser(final int index, final String userId) {
        Task task = findByIndexForUser(index, userId);
        if (task == null) return null;
        return task.getId();
    }

}
