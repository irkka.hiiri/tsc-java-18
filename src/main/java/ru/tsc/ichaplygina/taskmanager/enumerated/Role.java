package ru.tsc.ichaplygina.taskmanager.enumerated;

public enum Role {

    USER("User"),
    ADMIN("Admin");

    private final String displayName;

    Role(final String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }

}
