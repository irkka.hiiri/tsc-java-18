package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    private final static String NAME = "update task by index";

    private final static String DESCRIPTION = "update task by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        super.execute();
        final int index = readNumber(INDEX_INPUT);
        if (getTaskService().findByIndex(index - 1) == null) throw new TaskNotFoundException();
        final String name = readLine(NAME_INPUT);
        final String description = readLine(DESCRIPTION_INPUT);
        getTaskService().updateByIndex(index - 1, name, description);
        showUpdateResult();
    }

}
