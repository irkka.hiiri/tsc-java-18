package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.NAME_INPUT;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    private final static String NAME = "remove task by name";

    private final static String DESCRIPTION = "remove task by name";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        super.execute();
        final String name = readLine(NAME_INPUT);
        if (getTaskService().removeByName(name) == null) throw new TaskNotFoundException();
        showRemoveResult();
    }

}
