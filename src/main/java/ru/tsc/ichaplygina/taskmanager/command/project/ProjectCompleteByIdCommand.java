package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    private final static String NAME = "complete project by id";

    private final static String DESCRIPTION = "complete project by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        super.execute();
        final String id = readLine(ID_INPUT);
        getProjectService().completeById(id);
        showUpdateResult();
    }

}
