package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.TaskNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.INDEX_INPUT;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    private final static String NAME = "remove task by index";

    private final static String DESCRIPTION = "remove task by index";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        super.execute();
        final int index = readNumber(INDEX_INPUT);
        if (getTaskService().removeByIndex(index - 1) == null) throw new TaskNotFoundException();
        showRemoveResult();
    }

}
