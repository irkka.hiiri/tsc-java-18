package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import java.util.Comparator;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.NO_PROJECTS_FOUND;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectListCommand extends AbstractProjectCommand {

    private final static String NAME = "list projects";

    private final static String DESCRIPTION = "show all projects";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        super.execute();
        if (getProjectService().isEmpty()) {
            printLinesWithEmptyLine(NO_PROJECTS_FOUND);
            return;
        }
        final Comparator<Project> comparator = readComparator();
        printListWithIndexes(getProjectService().findAll(comparator));
    }

}
