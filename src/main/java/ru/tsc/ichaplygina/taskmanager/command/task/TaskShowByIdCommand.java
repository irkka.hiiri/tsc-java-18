package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.ID_INPUT;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public class TaskShowByIdCommand extends AbstractTaskCommand {

    private final static String NAME = "show task by id";

    private final static String DESCRIPTION = "show task by id";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        super.execute();
        final String id = readLine(ID_INPUT);
        final Task task = getTaskService().findById(id);
        showTask(task);
    }

}
