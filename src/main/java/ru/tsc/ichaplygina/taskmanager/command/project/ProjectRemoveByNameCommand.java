package ru.tsc.ichaplygina.taskmanager.command.project;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.ProjectNotFoundException;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.*;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    private final static String NAME = "remove project by name";

    private final static String DESCRIPTION = "remove project by name";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        super.execute();
        final String name = readLine(NAME_INPUT);
        if (getProjectTaskService().removeProjectByName(name) == null) throw new ProjectNotFoundException();
        showRemoveProjectResult();
    }

}
