package ru.tsc.ichaplygina.taskmanager.command.task;

import ru.tsc.ichaplygina.taskmanager.exception.AbstractException;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import java.util.Comparator;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.NO_TASKS_FOUND;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.*;

public class TaskListCommand extends AbstractTaskCommand {

    private final static String NAME = "list tasks";

    private final static String DESCRIPTION = "show all tasks";

    @Override
    public String getCommand() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        super.execute();
        if (getTaskService().isEmpty()) {
            printLinesWithEmptyLine(NO_TASKS_FOUND);
            return;
        }
        Comparator<Task> comparator = readComparator();
        printListWithIndexes(getTaskService().findAll(comparator));
    }

}
